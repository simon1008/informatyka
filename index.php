<!DOCTYPE html> 
<html> 
 <head> 
   <meta charset="utf-8"> 
   <title>Projekt bazy danych</title> 
   <link rel="Stylesheet"   type="text/css" href="style.css" />
   <meta name="description" content="Baza danych">
   <meta name="keywords"    content="baza danych">
   <meta name="author"      content="Ja">
   <meta name="viewport"    content="width=device-width, initial-scale=1.0">
 </head>
 <body>

  <div id="header">
    <h1>PROJEKT BAZY DANYCH</h1>
  </div>

  <div id="menu">
    <ul>
      <li><a href="index.php?id=home">Home</a></li>
      <li><a href="index.php?id=pokaz">Pokaż rekordy od 30 do 60</a></li>
      <li><a href="index.php?id=A">Rekordy z miejscowościami na A</a></li>
      <li><a href="index.php?id=1">1 grudnia 2000</a></li>
      <li><a href="index.php?id=10-18">10-18 lat</a></li>
      <li><a href="index.php?id=szukaj">Rekordy</a></li>
    </ul>
        
    </nav>
  </div>
  
  <div id="content">
    <p>
     <?php
        require('config.inc.php');
        // Create connection
        $conn = new mysqli($servername, $username, $password, $dbname);
        // Check connection
        if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
        } 
        echo "Connection success";
        
        $id = isset ($_GET['id'])?$_GET['id']: "";
        
        switch ($id)
        {
         case "home":   require('home.inc.html');       break;
         case "pokaz":  require('pokaz.inc.html');      break;
         case "A":      require ('A.inc.html');         break;
         case "1":      require ('1.inc.html');         break;
         case "10-18":  require ('10-18.inc.html');     break;
         case "szukaj": require ('szukaj.inc.html');    break;
         case "wyniki": require('wyniki.php');          break;
         default:       require ('home.inc.html');
        }
        mysqli_close($conn);
    ?> 
   </p>
  <div id="footer">
        &copy; 2018 Ja
  </div>

</body>
</html>									